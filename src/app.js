import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { getCookie, setCookie } from './utils/cookies/cookieHelper';
import { SERVICE_URL } from './api';
import LoginPage from './containers/login/index';
import LobbyPage from './containers/lobby/index';

const App = () => {
  const [loginId, setLoginId] = useState(null);

  const checkLoginStatus = async () => {
    const login = getCookie('loginId');
    if (login !== '') {
      const isLoginActive = await axios.get(`${SERVICE_URL}/checkLogin`, { params: { loginId: login } });
      if (!isLoginActive.data) {
        setCookie('loginId', '', 0);
        setLoginId('');
        alert('Login timed out, please log in again');
      } else {
        setLoginId(login);
      }
    } else {
      setLoginId('');
    }
  };

  useEffect(() => {
    checkLoginStatus();
  }, []);

  return (
    <div>
      {loginId === '' && loginId !== null && (
      <LoginPage />
      )}
      {loginId !== '' && loginId !== null && (
      <LobbyPage />
      )}
    </div>
  );
};

export default App;
