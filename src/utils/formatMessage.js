const formatMessage = (messageType, data) => {
  const message = {
    type: messageType,
    data,
  };
  return JSON.stringify(message);
};

// eslint-disable-next-line import/prefer-default-export
export { formatMessage };
