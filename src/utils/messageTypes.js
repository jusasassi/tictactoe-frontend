// eslint-disable-next-line import/prefer-default-export
export const MESSAGE_TYPE = {
  NONE: 0,
  CREATE_ROOM: 1,
  DISCONNECT: 2,
  MESSAGE_ROOM: 3,
  REMOVE_ROOM: 4,
  CONNECT_ROOM: 5,
};
