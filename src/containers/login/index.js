import React, { useState } from 'react';
import axios from 'axios';
import { setCookie } from '../../utils/cookies/cookieHelper';
import { SERVICE_URL } from '../../api';

const LoginPage = () => {
  const [loginName, setLoginName] = useState('');
  const [hasDuplicateName, setHasDuplicateName] = useState(false);

  const handleInput = (event) => {
    setLoginName(event.target.value);
  };

  const submitlogin = async (event) => {
    event.preventDefault();
    let login;
    try {
      login = await axios.get(`${SERVICE_URL}/createLogin`, { params: { userName: loginName } });
      setCookie('loginId', login.data.loginId, 1);
      window.location.reload();
    } catch (err) {
      console.log(err);
      setHasDuplicateName(true);
    }
  };

  return (
    <div>
      <form onSubmit={submitlogin}>
        <input type="text" name="userName" onChange={handleInput} />
        <input type="submit" value="Login" />
      </form>
      {hasDuplicateName && (
        <p>Name already taken</p>
      )}
    </div>
  );
};

export default LoginPage;
