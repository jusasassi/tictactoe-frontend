/* eslint-disable no-shadow */
/* eslint-disable react/jsx-one-expression-per-line */
import React, { useState, useEffect, useRef } from 'react';
import axios from 'axios';
import { setCookie, getCookie } from '../../utils/cookies/cookieHelper';
import { SERVICE_URL, SOCKET_URL } from '../../api';
import { MESSAGE_TYPE } from '../../utils/messageTypes';
import { formatMessage } from '../../utils/formatMessage';

let socket;
let connectedRoom;

const LobbyPage = () => {
  const [rooms, setRooms] = useState([]);
  const [roomNameInput, setRoomNameInput] = useState('');
  const [chatMessage, setChatMessage] = useState('');
  const [gameChat, setGameChat] = useState([]);
  const [isConnected, setIsConnected] = useState(false);

  const messagesEndRef = useRef(null);
  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView({ behavior: 'smooth' });
  };

  const leaveRoom = () => {
    if (connectedRoom !== undefined) {
      console.log(`left room ${connectedRoom.id}`);
      connectedRoom.socket.close();
      connectedRoom = undefined;
      setGameChat([]);
      setIsConnected(false);
    }
  };

  const createRoom = async (id) => {
    if (connectedRoom !== undefined) leaveRoom();
    setIsConnected(true);
    connectedRoom = {
      socket: new WebSocket(`${SOCKET_URL}/${id}`),
      id,
    };
    console.log(`Joined Room ${id}`);
    connectedRoom.socket.onmessage = (event) => {
      const parsedData = JSON.parse(event.data);
      console.log('message received');
      console.log(parsedData);
      switch (parsedData.type) {
        case MESSAGE_TYPE.MESSAGE_ROOM:
          console.log(parsedData.data.message);
          setGameChat((chat) => [...chat, parsedData.data.message]);
          scrollToBottom();
          break;
        case MESSAGE_TYPE.DISCONNECT:
          console.log('room closed');
          connectedRoom = undefined;
          setGameChat([]);
          setIsConnected(false);
          break;
        default:
          console.log('Unhandled message');
      }
    };

    connectedRoom.socket.onopen = () => {
      connectedRoom.socket.send(formatMessage(MESSAGE_TYPE.CONNECT_ROOM, { loginId: getCookie('loginId') }));
    };
  };

  const joinRoom = async (id) => {
    if (connectedRoom !== undefined && connectedRoom.id === id) {
      console.log('Already connected to the room');
    } else {
      await createRoom(id);
    }
  };

  useEffect(() => {
    if (socket === undefined) socket = new WebSocket(`${SOCKET_URL}/lobby`);

    socket.onmessage = (event) => {
      console.log('lobby message reveived');
      console.log(event.data);
      const parsedData = JSON.parse(event.data);
      switch (parsedData.type) {
        case MESSAGE_TYPE.CREATE_ROOM:
          setRooms(parsedData.data.rooms);
          break;
        case MESSAGE_TYPE.CONNECT_ROOM:
          console.log(parsedData.data.roomId);
          joinRoom(parsedData.data.roomId);
          break;
        default:
          console.log('unhandled message');
      }
    };
  });

  const handleInput = (event) => {
    setChatMessage(event.target.value);
  };

  const clearCookies = () => {
    setCookie('loginId', '', 0);
  };

  const logOut = async () => {
    const loginId = getCookie('loginId');
    await axios.get(`${SERVICE_URL}/clearLogin`, { params: { loginId } });
    clearCookies();
    window.location.reload();
  };

  const onsubmit = (event) => {
    event.preventDefault();
    const outgoingMessage = formatMessage(
      MESSAGE_TYPE.CREATE_ROOM,
      { name: roomNameInput, host: getCookie('loginId') },
    );
    socket.send(outgoingMessage);
    setRoomNameInput('');
  };

  const messageRoom = (event) => {
    event.preventDefault();
    if (connectedRoom !== undefined) {
      console.log('Send message');
      const sendMessage = formatMessage(
        MESSAGE_TYPE.MESSAGE_ROOM,
        { loginId: getCookie('loginId'), message: chatMessage },
      );
      connectedRoom.socket.send(sendMessage);
      setChatMessage('');
    }
  };

  const deleteRoom = () => {
    if (connectedRoom !== undefined) {
      console.log(`deleted room ${connectedRoom.id}`);
      const sendMessage = formatMessage(MESSAGE_TYPE.REMOVE_ROOM);
      connectedRoom.socket.send(sendMessage);
    }
  };

  const onEnterPress = (event) => {
    if (event.keyCode === 13 && event.shiftKey === false) {
      messageRoom(event);
    }
  };

  const handleRoomNameInput = (event) => {
    setRoomNameInput(event.target.value);
  };

  const divStyle = {
    overflowY: 'scroll',
    flex: 1,
    flexWrap: 'wrap',
    border: '1px solid red',
    height: '700px',
    width: '500px',
    position: 'relative',
  };

  return (
    <div>
      <button type="button" onClick={logOut}>Log Out</button>
      <p>Logged in</p>
      {!isConnected && (
      <div>
        <form onSubmit={onsubmit}>
          <input type="text" value={roomNameInput} onChange={handleRoomNameInput} />
          <input type="submit" value="Create Room" />
        </form>
        {rooms.length > 0 && (
          <ul>
            {rooms.map((room) => (
              <li>
                <button type="button" onClick={() => joinRoom(room.id)}>
                  {room.name}
                </button>
              </li>
            ))}
          </ul>
        )}
      </div>
      )}
      {isConnected && (
        <div>
          <button type="button" onClick={leaveRoom}>Leave Room</button>
          <button type="button" onClick={deleteRoom}>Delete Room</button>
          {gameChat.length > 0 && (
          <div style={divStyle}>
            {gameChat.map((message) => (
              <p>{message}</p>
            ))}
            <div ref={messagesEndRef} />
            <br />
          </div>
          )}
          <form onSubmit={messageRoom}>
            <textarea rows="2" cols="67" name="userName" onChange={handleInput} value={chatMessage} onKeyDown={onEnterPress} />
            <br />
            <input type="submit" value="Send Message" />
          </form>
        </div>
      )}
    </div>
  );
};

export default LobbyPage;
