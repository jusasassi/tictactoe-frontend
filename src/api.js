export const SERVICE_URL = process.env.REACT_APP_SERVICE_URL;
export const SOCKET_URL = process.env.REACT_APP_SOCKET_URL;
