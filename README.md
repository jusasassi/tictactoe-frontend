Frontend for TicTacToe program

How to start

confirm you have npm installed

1. Download/Clone repository
2. CD to the root
3. run 'npm install' to install dependecies
4. copy config/config.sample and change parameters to match where the backend is hosted
5. run 'source config/file' where file is the copy fron config.sample
6. run 'npm start'
